#45c6e762a8d27c943d0c5dcd17781cd0f415bc21379551ebc3b8f6d15c44d4fccd6394752f5c710abd059
#photo-188325942_457239019
import vk
from random import randint
from database import database
import weather
import time

class Bot:
    def __init__(self, token):
        self.session = vk.Session(access_token = token)
        self.api = vk.API(self.session)
        self.db = database('vkbot.db')
        self.users = self.db.get_user(all_users=True)
        self.greeting = 'Привет, {0}\nЯ бот такой-то лалала...\n' #каждый заменяет под себя
        self.functions = { '0' : self.state_0, '1' : self.state_1, '11' : self.state_11, '12' :  self.state_12, '13' : self.state_13, '14' : self.state_14 }
        print(self.users)
        self.mainLoop()

    
    def mainLoop(self):
        while True:
            time.sleep(0.1)
            dialogs = self.api.messages.getDialogs(unanswered=1, v=5.92)
            if dialogs['count'] != 0:
                user_id = str( dialogs['items'][0]['message']['user_id'] )
                message = dialogs['items'][0]['message']['body']
                user_info = self.api.users.get(user_ids=user_id, v=5.92)
                user_name = user_info[0]['first_name']
                self.checkUser(user_id, user_name)
                message = self.editMessage(message, user_id)
                print(self.users)
                print( self.sendMessage(message, user_id) ) 
                
                
    def checkUser(self, user_id, user_name):
        if user_id not in self.users:
            self.users[user_id] = [user_name, '0', '']
            if self.db.add_user(user_id, user_name):
                print( 'Пользователь {0} - {1} добавлен'.format(user_id, user_name) )
            else:
                print( 'Пользователь {0} - {1} не добавлен'.format(user_id, user_name) )          

    def state_0(self, args):
        self.users[args['user_id']][1] = '1'
        ans = self.greeting.format( args['username'] )
        for com in args['commands']:
            ans += '{0}. {1} \n'.format(com[1:], args['commands'][com][0]) #com[1:] + ' ' + commands[com][0] + '\n'
        return ans , ''

    def state_1(self, args):
        if args['message'].lower().strip() in ['1', 'получить погоду']:
            self.users[args['user_id']][1] = '11'
            return 'Введите название города', ''
        elif args['message'].lower().strip() in ['2', 'получить прогноз']:
            self.users[args['user_id']][1] = '12'
            return 'Введите название города', ''
        elif args['message'].lower().strip() in ['3', 'свободное общение']:
            self.users[args['user_id']][1] = '13'
            return 'Это режим свободное общение, можно выйти с помощью этих комманд: Стоп, Выход', ''
        elif args['message'].lower().strip() in ['4', 'поиск фильма']:
            self.users[args['user_id']][1] = '14'
            return "Из этого режима можно выйти с помощью команд: Выход, Стоп"
            return "Введите жанр фильма", ''
        else:
            ans = ''
            for com in args['commands']:
                ans += '{0}. {1} \n'.format(com[1:], args['commands'][com][0]) #com[1:] + ' ' + commands[com][0] + '\n'
            return ans , ''

    def state_11(self, args):
        w = weather.get_weather(args['message'], weather.key)
        if 'current_temp' in w:
            ans = 'Сегодня в городе {0}: \n Температура: {1} \n Погода: {2}'.format(args['message'],
                                                                                    w['current_temp'],
                                                                                    w['w_desc'])
            self.users[args['user_id']][1] = '1'
            return ans, '' 
        else:
            return w + '\n Введите название города еще раз', ''

    def state_12(self, args):
        w = weather.get_forecast(args['message'], weather.key)
        if type(w) == type(list()):
            w = w[ : : 4]
            template = '{3} в городе {0}: \n Температура: {1} \n Погода: {2}\n\n'
            ans = ""
            for point_w in w:
                ans += template.format(args['message'],
                                                point_w['current_temp'],
                                                point_w['w_desc'],
                                                point_w["timestamp"])
            self.users[args['user_id']][1] = '1'
            return ans, ""
        else:
            self.users[args['user_id']][1] = '1'
            return w, ""

    def state_13(self, args):
        if args["message"].lower().strip() in["стоп", "выход"]:
            self.users[args['user_id']][1] = '1'
            return "Свободное общение закончилось"
        else:
            ans = self.db.get_answer(args["message"])
            if ans:
                print(ans)
                return ans[1], ans[2]
            else:
                return "Я тоби ни панимаю", ""

    def state_14(self,args):
        if args["message"].lower().strip() in["выход", "стоп"]:
            self.users[args['user_id']][1] = '1'
            return "Режим поиска фильмов закончен"
        if args["message"].lower().strip() in ["фантастика"]:
            return "Фантастика: Интерстеллар, Грань будущего, Первому игроку приготовиться, Пятый элемент, Матрица, Аватар, Начало, Чужой,\n Звёздные войны: Эпизод 1,\n Звёздные войны: Эпизод 2,\n Звёздные войны: Эпизод 3,\n Звёздные войны: Эпизод 4,\n Звёздные войны: Эпизод 5,\n Звёздные войны: Эпизод 6", ""
        if args["message"].lower().strip() in["детектив"]:
            return "Детективы: Шерлок Холмс и доктор Ватсон, Птица с Хрустальным оперением, Игра на вылет, Разговор, Молчание ягнят, Мальтийский сокол, Ошибка резидента", ""
        else:
            return "Я такого жанра не знаю",""
        

    
    def editMessage(self, message, user_id):
        answer = { 'value' : 'д', 'att' : '' }
        user_state = self.users[user_id][1]
        commands = self.db.get_commands(user_state)
        args = {}
        args['commands'] = commands
        args['username'] = self.users[user_id][0]
        args['message'] = message
        args['user_id'] = user_id
        if user_state in self.functions:
            answer['value'], answer['att'] = self.functions[user_state](args)
            print(answer)
        return answer
    
    def sendMessage(self, message, user_id):
        try:
            self.api.messages.send( user_id=user_id, random_id = randint(0, 100000),
                       message = message['value'], attachment = message['att'] , v=5.92)
            return 'Сообщение отправлено'
        except:
            return 'Не удалось отправить сообщение пользователю {0}'.format(user_id)



b = Bot(token='fae52b4975b6b56d8b5d3abe3e00a3c945832a8af2cf89ca5e7f915f9ec3f1b615414b3e4e32f7e24c27a')






