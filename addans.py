from database import database


db = database('vkbot.db')

def addans(db): #функция добавления шаблона ответа в таблицу answers
    temp = input('Введите сообщение пользователя ')
    ans = input('Введите ответ на сообщение ')
    is_att = input('Является ли вложением(0/1) ')
    if db.add_answer(temp, ans, is_att) == True: #Если добавление записи не вызвало ошибку
        return 'Сообщение добавлено'
    else: #Если возникла ошибка
        return 'Не удалось добавить сообщение'

def addcom(db): #Функция добавления записи в таблицу Commands
    code = input('Введите код команды ')
    command = input('Введите команду ')
    state = input('Введите состояние пользователя ')

    if db.add_command(code, command, state) == True: #Если добавление записи не вызвало ошибку
        return 'Сообщение добавлено'
    else: #Если вызвало
        return 'Не удалось добавить сообщение'

while True:
    print('1. Добавить новый шаблон')
    print('2. Добавить новую команду')
    a = input('Сделайте выбор ')
    if a == '1':
        print( addans(db) )
    elif a == '2':
        print( addcom(db) )
    else:
        break
