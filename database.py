import sqlite3


class database:
    def __init__(self, filename):
        self.connection = sqlite3.connect( filename )
        self.cursor = self.connection.cursor()
        print( self.check_users() )
        print( self.check_messages() )
        print( self.check_answers() )
        print( self.check_commands() )

    def check_users(self):
        try:
            self.cursor.execute('SELECT * FROM users')
            return 'Table USERS already exists'
        except:
            self.cursor.execute('''CREATE TABLE users (user_id varchar(25) primary key,    
                                user_name varchar(25), state varchar(2),
                                last_message varchar(255) )''')
            return 'Creating table USERS'

    def check_commands(self):
        try:
            self.cursor.execute('SELECT * FROM commands')
            return 'Table COMMANDS already exists'
        except:
            self.cursor.execute('''CREATE TABLE commands (code varchar(2) primary key,    
                                command varchar(255), state varchar(2) )''')
            return 'Creating table COMMANDS'
    
    def check_answers(self):
        try:
            self.cursor.execute('SELECT * FROM answers')
            print( self.cursor.fetchall() )
            return 'Table ANSWERS already exists'
        except:
            self.cursor.execute('''CREATE TABLE answers (template varchar(255) primary key,
                                answer varchar(255), attachment varchar(255) )''')
            return 'Creating table ANSWERS'
        
    def check_messages(self):
        try:
            self.cursor.execute('SELECT * FROM messages')
            return 'Table MESSAGES already exists'
        except:
            self.cursor.execute('''CREATE TABLE messages (m_id integer primary key AUTOINCREMENT,
                                m_text varchar(255),  is_command bool, user_id varchar(25)) ''')
            return 'Creating table MESSAGES'

    def get_user(self, user_id=0, all_users=False):
        if all_users: #Если поступил запрос на получение данных о всех пользователях в таблице
            self.cursor.execute('SELECT * FROM users')
        else:
            self.cursor.execute('SELECT * FROM users WHERE user_id="{0}"'.format(user_id))
        ans = self.cursor.fetchall()
        users = {}
        for user in ans:
            users[user[0]] = list( user[1:] )
        return users

    def add_user(self, user_id, user_name, status=0, last_message=''):
        try:
            self.cursor.execute('INSERT INTO users VALUES ( "{0}", "{1}", "{2}", "{3}" )'.format(user_id, user_name, status, last_message))
            self.connection.commit()
            return True
        except:
            return False

    def modify_user(self, user_id, user_name, status, last_message):
        try:
            self.cursor.execute('UPDATE users SET user_name="{1}", status="{2}", last_message="{3}" WHERE user_id="{0}"'.format(user_id, user_name, status, last_message))
            self.connection.commit()
            return True
        except:
            return False
        
    def get_message(self, user_id=0, all_users=False):
        if all_users: #Если поступил запрос на получение данных о всех пользователях в таблице
            self.cursor.execute('SELECT * FROM users')
        else:
            self.cursor.execute('SELECT * FROM messages WHERE user_id="{0}"'.format(user_id))
        ans = self.cursor.fetchall()
        return ans

    def get_answer(self, message):
        self.cursor.execute('SELECT * FROM answers WHERE template="{0}"'.format(message))
        ans = self.cursor.fetchall()
        if len(ans) == 0:
            ans = False
            return ans
        return ans[0]

    def get_commands(self, state):
        self.cursor.execute('SELECT * FROM commands WHERE state="{0}"'.format(state))
        coms = self.cursor.fetchall()
        if len(coms) == 0:
            ans = {}
        else:
            ans = {}
            for c in coms:
                ans[ c[0] ] = [ c[1], c[2] ]
        return ans

    def add_answer(self, template, answer, attachment):
        try:
            self.cursor.execute( 'INSERT INTO answers VALUES ("{0}", "{1}", "{2}")'.format(template, answer, attachment) )
            self.connection.commit()
            return True
        except:
            return False

    def add_command(self, code, command, state):
        try:
            self.cursor.execute( 'INSERT INTO commands VALUES ("{0}", "{1}", "{2}")'.format(code, command, state) )
            self.connection.commit()
            return True
        except:
            return False

        
#db = database('vkbot.db')
